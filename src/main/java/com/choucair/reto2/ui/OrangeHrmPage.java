package com.choucair.reto2.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class OrangeHrmPage extends PageObject{
	
	public static final Target TXT_CAMPO_USUARIO = Target.the("Campo donde se ingresa el nombre de usuario").located(By.xpath("//*[@id='login']/form/input[1]"));
	public static final Target BTN_LOGIN = Target.the("boton para realizar login").located(By.xpath("//*[@id=\"btnLogin\"]"));
	
}
