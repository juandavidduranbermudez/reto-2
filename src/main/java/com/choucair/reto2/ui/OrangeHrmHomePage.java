package com.choucair.reto2.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class OrangeHrmHomePage extends PageObject{

	public static final Target MENU_PIM = Target.the("Menu PIM").located(By.id("menu_pim_viewPimModule"));
	public static final Target SUB_MENU_ADD_EMPLOYEE = Target.the("SUB Menu AGREGAR EMPLEADO").located(By.xpath("//a[@ui-sref=\"pim.add_employee\"]"));
	public static final Target SUB_MENU_EMPLOYEE_LIST = Target.the("SUB Menu AGREGAR EMPLEADO").located(By.id("menu_pim_viewEmployeeList"));

}
