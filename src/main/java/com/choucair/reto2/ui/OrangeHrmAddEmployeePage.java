package com.choucair.reto2.ui;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;

public class OrangeHrmAddEmployeePage {
	
	private OrangeHrmAddEmployeePage() {
		throw new IllegalStateException("Utility class");
	}

	public static final Target TXT_FIRST_NAME = Target.the("Input para introducir primer nombre").located(By.id("firstName"));
	public static final Target MIDDLE_NAME    = Target.the("Input para introducir segundo nombre").located(By.id("middleName"));
	public static final Target LAST_NAME      = Target.the("Input para introducir primer apellido").located(By.id("lastName"));
	public static final Target EMPLOYEE_ID    = Target.the("Input para introducir id del empleado").located(By.id("employeeId"));
	public static final Target LOCATION       = Target.the("Input para introducir localizacion").located(By.xpath("//input[contains(@data-activates, \"select-options\") and @value=\"-- Select --\"]"));
	public static final Target LOCATION_LIST  = Target.the("Input para introducir localizacion").located(By.xpath("//ul[contains(@id, \"select-options\") and @style]"));
	public static final Target BTN_SAVE       = Target.the("Input para introducir localizacion").located(By.id("systemUserSaveBtn"));
	public static final Target OTHER_ID       = Target.the("Input para introducir localizacion").located(By.id("other_id"));
	public static final Target DATE_OF_BIRTH  = Target.the("Input para introducir localizacion").located(By.id("date_of_birth"));
	
	public static final Target MARITAL_STATUS       = Target.the("Input para introducir localizacion").located(By.id("marital_status_inputfileddiv"));
	public static final Target MARITAL_STATUS_LIST  = Target.the("Input para introducir localizacion").located(By.xpath("//div[@id=\"marital_status_inputfileddiv\"]//ul"));
	
	public static final Target NATIONALITY  = Target.the("Input para introducir localizacion").located(By.id("nationality_inputfileddiv"));
	public static final Target NATIONALITY_LIST  = Target.the("Input para introducir localizacion").located(By.xpath("//div[@id=\"nationality_inputfileddiv\"]//ul"));
	
	public static final Target GENDER  = Target.the("Input para introducir localizacion").located(By.xpath("//label[@for=\"gender_1\"]"));
	
	public static final Target DRIVER_LICENSE_NUMBER   = Target.the("Input para introducir localizacion").located(By.id("driver_license"));
	public static final Target LICENSE_EXPIRY_DATE     = Target.the("Input para introducir localizacion").located(By.id("license_expiry_date"));
	public static final Target NICK_NAME               = Target.the("Input para introducir localizacion").located(By.id("nickName"));
	public static final Target MILITARY_SERVICE        = Target.the("Input para introducir localizacion").located(By.id("militaryService"));
	public static final Target BTN_SAVE_2              = Target.the("Input para introducir localizacion").located(By.xpath("//*[@id=\"content\"]/div[2]/ui-view/div[2]/div/custom-fields-panel/div/form/materializecss-decorator[3]/div/sf-decorator/div/button"));
														            
	
	
	public static final Target BLOOD_GROUP       = Target.the("Input para introducir localizacion").located(By.id("1_inputfileddiv"));
	public static final Target BLOOD_GROUP_LIST  = Target.the("Input para introducir localizacion").located(By.xpath("//div[@id=\"1_inputfileddiv\"]//ul"));
	
	public static final Target HOBBIES    = Target.the("Input para introducir localizacion").located(By.id("5"));
	public static final Target BTN_SAVE_3 = Target.the("Input para introducir localizacion").located(By.xpath("//*[@id=\"content\"]/div[2]/ui-view/div[2]/div/custom-fields-panel/div/form/materializecss-decorator[3]/div/sf-decorator/div/button"));
	
}

