package com.choucair.reto2.tasks;

import java.util.List;

import com.choucair.reto2.interactions.ClickOnElement;
import com.choucair.reto2.interactions.SelectList;
import com.choucair.reto2.interactions.Wait;
import com.choucair.reto2.ui.OrangeHrmAddEmployeePage;
import com.choucair.reto2.ui.OrangeHrmHomePage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class RealizarIngreso implements Task{
	
	private List<String> datos;
	
	public RealizarIngreso(List<String> datos) {
		this.datos = datos;
	}

	@Override
	
	public <T extends Actor> void performAs(T actor) {

		actor.attemptsTo(Click.on(OrangeHrmHomePage.MENU_PIM));
		actor.attemptsTo(Click.on(OrangeHrmHomePage.SUB_MENU_ADD_EMPLOYEE));
		
		actor.attemptsTo(Wait.element(OrangeHrmAddEmployeePage.TXT_FIRST_NAME));
		actor.attemptsTo(Enter.theValue(datos.get(0)).into(OrangeHrmAddEmployeePage.TXT_FIRST_NAME));
		actor.attemptsTo(Enter.theValue(datos.get(1)).into(OrangeHrmAddEmployeePage.MIDDLE_NAME));
		actor.attemptsTo(Enter.theValue(datos.get(2)).into(OrangeHrmAddEmployeePage.LAST_NAME));

		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.LOCATION));
		actor.attemptsTo(SelectList.byVisibleText(datos.get(4), OrangeHrmAddEmployeePage.LOCATION_LIST));
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.BTN_SAVE));
		
		actor.attemptsTo(Wait.element(OrangeHrmAddEmployeePage.OTHER_ID));
		actor.attemptsTo(Enter.theValue(datos.get(5)).into(OrangeHrmAddEmployeePage.OTHER_ID));
		actor.attemptsTo(Enter.theValue(datos.get(6)).into(OrangeHrmAddEmployeePage.DATE_OF_BIRTH));
		
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.MARITAL_STATUS));
		actor.attemptsTo(SelectList.byVisibleText(datos.get(7), OrangeHrmAddEmployeePage.MARITAL_STATUS_LIST));
		
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.NATIONALITY));
		actor.attemptsTo(SelectList.byVisibleText(datos.get(8), OrangeHrmAddEmployeePage.NATIONALITY_LIST));
		
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.GENDER));
		
		actor.attemptsTo(Enter.theValue(datos.get(9)).into(OrangeHrmAddEmployeePage.DRIVER_LICENSE_NUMBER));
		actor.attemptsTo(Enter.theValue(datos.get(10)).into(OrangeHrmAddEmployeePage.LICENSE_EXPIRY_DATE));
		actor.attemptsTo(Enter.theValue(datos.get(11)).into(OrangeHrmAddEmployeePage.NICK_NAME));
		actor.attemptsTo(Enter.theValue(datos.get(12)).into(OrangeHrmAddEmployeePage.MILITARY_SERVICE));
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.BTN_SAVE_2));
		
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.BLOOD_GROUP));
		actor.attemptsTo(SelectList.byVisibleText(datos.get(13), OrangeHrmAddEmployeePage.BLOOD_GROUP_LIST));
		
		actor.attemptsTo(Enter.theValue(datos.get(14)).into(OrangeHrmAddEmployeePage.HOBBIES));
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.BTN_SAVE_3));
		
		actor.attemptsTo(ClickOnElement.noClickable(OrangeHrmHomePage.SUB_MENU_EMPLOYEE_LIST,new OrangeHrmHomePage()));
		
		
		
		
			
		
	}

	public static RealizarIngreso delRegistro(List<String> datos) {

		return new RealizarIngreso(datos);
	}

}
