package com.choucair.reto2.tasks;

import com.choucair.reto2.ui.OrangeHrmPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar implements Task{

	@Override
	public <T extends Actor> void performAs(T actor) {

		actor.attemptsTo(Open.browserOn(new OrangeHrmPage()));
		actor.attemptsTo(Click.on(OrangeHrmPage.BTN_LOGIN));
	}

	public static Ingresar enLaPaginaOrageHR() {
	
		return new Ingresar();
	}

}
