package com.choucair.reto2.interactions;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.exceptions.SerenityManagedException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;

public class Wait implements Interaction{
	
	private Target webElement;

	public Wait(Target webElement) {
		this.webElement = webElement;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		esperarElemento(webElement.resolveFor(actor));
	}

	public static Wait element(Target webElement) {
		
		return new Wait(webElement);
	}
	
	private void esperarElemento(WebElement element) {
		boolean enEspera=false;
		while (!enEspera) {
			enEspera=existsElement(element);
		}
	}
	
	private boolean existsElement(WebElement element) {
		try {
			element.getText();
		} catch (SerenityManagedException|NoSuchElementException e) {
			return false;
		}
		return true;
	}

	
}
