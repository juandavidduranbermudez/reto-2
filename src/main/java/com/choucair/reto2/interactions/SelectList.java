package com.choucair.reto2.interactions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;

public class SelectList implements Interaction{

	private  String opcion;
	private Target locationList;
	
	public SelectList(String opcion, Target locationList) {
		this.opcion = opcion;
		this.locationList = locationList;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		List<WebElement> elements = locationList.resolveFor(actor).findElements(By.tagName("li"));
		
		for (WebElement webElement : elements) {
			if (webElement.getText().replace(" ","").equals(opcion.replace(" ", ""))) {
				webElement.click();
				break;
			}
			
		}
		
	}

	public static SelectList byVisibleText(String opcion, Target locationList) {

		return new SelectList(opcion, locationList);
	}

}
