package com.choucair.reto2.interactions;

import org.openqa.selenium.JavascriptExecutor;

import com.choucair.reto2.ui.OrangeHrmHomePage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;

public class ClickOnElement implements Interaction {

	private Target subMenuEmployeeList;
	private OrangeHrmHomePage orangeHrmHomePage;
	
	
	
	public ClickOnElement(Target subMenuEmployeeList, OrangeHrmHomePage orangeHrmHomePage) {
		this.subMenuEmployeeList = subMenuEmployeeList;
		this.orangeHrmHomePage = orangeHrmHomePage;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {

		JavascriptExecutor executor = (JavascriptExecutor)orangeHrmHomePage.getDriver();
		executor.executeScript("arguments[0].click();", subMenuEmployeeList.resolveFor(actor));

	}

	public static ClickOnElement noClickable(Target subMenuEmployeeList, OrangeHrmHomePage orangeHrmHomePage) {

		return new ClickOnElement(subMenuEmployeeList, orangeHrmHomePage);
	}

}
