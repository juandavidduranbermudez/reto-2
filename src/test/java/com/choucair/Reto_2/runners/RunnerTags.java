package com.choucair.Reto_2.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/OrageHR_Validation.feature",tags= "@loguin",glue="com.choucair.Reto_2.stepdefinitions",snippets=SnippetType.CAMELCASE)
public class RunnerTags {

}

