package com.choucair.Reto_2.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import com.choucair.reto2.tasks.Ingresar;
import com.choucair.reto2.tasks.RealizarIngreso;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class OrangeHrmDefinitions {

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor usuarioOrangeHrm = Actor.named("Juan");
	
	@Before
	public void congiguracionInicial() {
		// TODO Auto-generated method stub
		usuarioOrangeHrm.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^que Juan necesita crear un empleado en el OrageHR$")
	public void queJuanNecesitaCrearUnEmpleadoEnElOrageHR() throws Exception {
		// Write code here that turns the phrase above into concrete actions
		usuarioOrangeHrm.wasAbleTo(Ingresar.enLaPaginaOrageHR());
		
	}


	
	@When("^el realiza el ingreso del registro en la aplicación$")
	public void el_realiza_el_ingreso_del_registro_en_la_aplicación(DataTable data) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		List<List<String>> datos=data.raw();
		System.out.println(datos);
		usuarioOrangeHrm.attemptsTo(RealizarIngreso.delRegistro(datos.get(1)));
	    
	}
	

	
	
	@Then("^el visualiza el nuevo empleado en el aplicativo (.*)$")
	public void el_visualiza_el_nuevo_empleado_en_el_aplicativo_juan_david_duran(String nuevoIngreso) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		System.out.println(nuevoIngreso);
	}
	    
	    
	

}
